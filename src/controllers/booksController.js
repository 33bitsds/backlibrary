import BooksModel from '../models/booksModel';

exports.createBook = async (req, res, next) => {
    try {

        let { titulo, autor } = req.body

        BooksModel.create({
            titulo: titulo,
            autor: autor,
            estatus: 1,
            usuario: 0
          }).then( data => {
            let book = data.get({ plain:true });
      
            if(!book){
                res.status(200).json({
                    resp: 200,
                    description: 'Datos incorrectos',
                })
                return
            }
            res.status(200).json({
                resp: 200,
                description: 'Libro registrado',
            })
            return
      
            
      
          }, error => {
            console.log('er: ', error)
            res.status(400).json({
                resp: 400,
                description: 'Error al crear libro',
            })
            return
          })
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.editBook = async ( req, res, next ) => {
    try {

        let {id, titulo, autor, estatus } = req.body
        BooksModel.findOne({where: {id: id}}).then ( data => {
            if(data){

                data.update({
                    titulo: titulo,
                    autor: autor,
                    estatus: estatus,

                }).then( result => {
                    if(!result){
                        res.status(200).json({
                            resp: 200,
                            description: 'Error al actualizar libro',
                        })
                        return  
                    }
                    res.status(200).json({
                        resp: 200,
                        description: 'Libro actualizado',

                    })
                    return 
                }, error => {
                    console.error('Error: ' + error)
                    res.status(400).json({
                        resp: 400,
                        description: 'Error al editar registro'
                    })
                    return
                })
            }else {
                res.status(400).json({
                    resp: 400,
                    description: 'Libro no encontrado',
                })
                return  
            }
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al encontrar registro'
            })
            return
        })

        
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.deleteBook = async ( req, res, next ) => {
    try {
        let {id} = req.body

        BooksModel.destroy({where: {id: id}}).then( data => {
            if(data == 1){
                res.status(200).json({
                    resp: 200,
                    description: 'Registro eliminado',
    
                })
                return 
            }
            else{
                res.status(400).json({
                    resp: 400,
                    description: 'Registro no encontrado',
    
                })
                return   
            }
            
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al eliminar registro'
            })
            return
        })
        
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.statusBook = async ( req, res, next ) => {
    try {
        let {id, estatus, usuario} = req.body
        if(estatus == '1'){
            usuario == 0
        }
        BooksModel.findOne({where: {id: id}}).then ( data => {
            if(data){

                data.update({
                    estatus: estatus,
                    usuario: usuario

                }).then( result => {
                    if(!result){
                        res.status(200).json({
                            resp: 200,
                            description: 'Error al asignar libro',
                        })
                        return  
                    }
                    // devolucion
                    if(estatus == 1){
                        
                    }else{

                    }
                    
                    res.status(200).json({
                        resp: 200,
                        description: 'Libro asignado',

                    })
                    return 
                }, error => {
                    console.error('Error: ' + error)
                    res.status(400).json({
                        resp: 400,
                        description: 'Error al asignar libro'
                    })
                    return
                })
            }else {
                res.status(400).json({
                    resp: 400,
                    description: 'Libro no encontrado',
                })
                return  
            }
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al encontrar registro'
            })
            return
        })

    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.allBooks = async ( req, res, next ) => {
    try {
        BooksModel.findAll({}).then( data => {
            if(data == []){
                res.status(200).json({
                    resp: 200,
                    description: 'No hay libros registrados',
    
                })
                return
            }else{
                console.log('Books ', data);
                res.status(200).json({
                    resp: 200,
                    description: 'Libros cargados',
                    books: data
                })
                return
            }
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al cargar  registros'
            })
            return
        })
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.byUser = async (req, res, next) => {
    try {
        BooksModel.findAll({where: {usuario: req.query.id}}).then( data => {
            if(data == []){
                res.status(200).json({
                    resp: 200,
                    description: 'No hay libros solicitados',
    
                })
                return
            }else{
                console.log('Books ', data);
                res.status(200).json({
                    resp: 200,
                    description: 'Libros cargados',
                    books: data
                })
                return
            }
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al cargar  registros'
            })
            return
        })
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}