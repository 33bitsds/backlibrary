import UsersModel from '../models/usersModel';
import { authenticationToken } from '../middlewares/authenticate';

exports.createUser = async (req, res, next) => {
    try {
        let { nombre, apellido, correo, password, tipo } = req.body
        
        UsersModel.create({
            nombre: nombre,
            apellido: apellido,
            correo: correo,
            password: password,
            tipo: tipo
        }).then( data => {
            let user  = data.get({ plain:true });
            if(!user){
                res.status(200).json({
                    resp: 200,
                    description: 'Datos incorrectos',
                })
                return
            }
            res.status(200).json({
                resp: 200,
                description: 'Ususario creado',
            })
            return
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al crear usuario'
            })
            return
        })

    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }

}

exports.login = async (req, res, next) => {
    try {
        let { correo, password } = req.body
        UsersModel.findOne({
            where: {correo: correo, password: password}
        }).then( data => {
            if(!data){
                res.status(200).json({
                    resp: 400,
                    description: 'Datos incorrectos',
                })
                return
            }
            else{
                const token = authenticationToken(data.dataValues)
                res.status(200).json({
                    resp: 200,
                    description: 'Datos correctos',
                    user: data.dataValues,
                    token: token
                })
                return 
            }
            

        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al ingresar'
            })
            return
        })

    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.editUser = async (req, res, next) => {
    try {
        let { id, nombre, apellido, correo, password, tipo } = req.body
        console.log('body ', req.body)
        UsersModel.findOne({where: {id: id}}).then ( data => {
            if(data){
                console.log('edit ', data)
                data.update({
                    nombre: nombre,
                    apellido: apellido,
                    correo: correo,
                    password: password,
                    tipo: tipo

                }).then( result => {
                    console.log('.--------------------------------.', result)
                    if(!result){
                        res.status(200).json({
                            resp: 200,
                            description: 'Error al actualizar usuario',
                        })
                        return  
                    }
                    res.status(200).json({
                        resp: 200,
                        description: 'Usuario actualizado',

                    })
                    return 
                }, error => {
                    console.error('Error: ' + error)
                    res.status(400).json({
                        resp: 400,
                        description: 'Error al editar registro'
                    })
                    return
                })
            }else {
                res.status(400).json({
                    resp: 400,
                    description: 'Usuario no encontrado',
                })
                return  
            }
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al encontrar registro'
            })
            return
        })
        
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.deleteUser = async (req, res, next) => {
    try {
        let {id} = req.body

        UsersModel.destroy({where: {id: id}}).then( data => {
            if(data == 1){
                res.status(200).json({
                    resp: 200,
                    description: 'Registro eliminado',
    
                })
                return 
            }
            else{
                res.status(400).json({
                    resp: 400,
                    description: 'Registro no encontrado',
    
                })
                return   
            }
            
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al eliminar registro'
            })
            return
        })
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}

exports.allUsers = async (req, res, next) => {
    try {
        UsersModel.findAll({}).then( data => {
            if(data == []){
                res.status(200).json({
                    resp: 200,
                    description: 'No hay usuarios registrados',
    
                })
                return
            }else{
                res.status(200).json({
                    resp: 200,
                    description: 'Usuarios cargados',
                    users: data
                })
                return
            }
        }, error => {
            console.error('Error: ' + error)
            res.status(400).json({
                resp: 400,
                description: 'Error al cargar  registros'
            })
            return
        })
    } catch (error) {
        console.log('Error: ' + error)
        next(error)
        return
    }
}