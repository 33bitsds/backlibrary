import express from 'express'
import cors from 'cors'
import { deploy } from './config/environment'
import bodyParser from 'body-parser'
import fs from 'fs'


const routes = fs.readdirSync(__dirname + '/routes')
const router = express.Router();
const app = express()

app.set('port', process.env.port || deploy.port)
let port = app.get('port')

const corsOptions = {
  allowedHeaders: ["Content-Type", "Authorization", "Access-Control-Allow-Methods", "Access-Control-Request-Headers"],
  credentials: true,
  enablePreflight: true
}


app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.options('*', cors(corsOptions))




// Routes
routes.map(fileNameRoutes => {
    let prefix = fileNameRoutes.split('.')[0]
    let baseUrl =
      prefix === 'index' ?
        `/library/v1/` :
        `/library/v1/${prefix}/`
    app.use(baseUrl, [], require(`./routes/${fileNameRoutes}`))
})
  
app.use(router.all('*', (req, res) => {
  res.status(404).send({ err: 'Not found' })
}))
  




app.listen(port, () => {
    console.log("Library services on port ", port);
});

module.exports = app;