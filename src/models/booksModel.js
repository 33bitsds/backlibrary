import { sequelize, model, sq, operator } from '../config/database';

export default class BooksModel extends model{ }

BooksModel.init({
    id: {
        primaryKey: true,  
        type: sq.INTEGER,
        allowNull: false,
        autoIncrement: true,
              
    },
    titulo: {
        type: sq.STRING,
        
    },
    autor: {
        type: sq.STRING,
    },
    estatus: {
        type: sq.INTEGER,
    },
    usuario: {
        type: sq.INTEGER
    }
},
{
  sequelize,
  tableName: 'libros',
});

