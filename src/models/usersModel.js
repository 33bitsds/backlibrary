import { sequelize, model, sq, operator } from '../config/database';

export default class UsersModel extends model{ }

UsersModel.init({
    id: {
        type: sq.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre: {
        type: sq.STRING,
        allowNull: false,
        
    },
    apellido: {
        type: sq.STRING,
        allowNull: false
    },
    correo: {
        type: sq.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: sq.STRING,
        allowNull: false
    },
    tipo: {
        type: sq.STRING,
        allowNull: false
    }
},
{
    sequelize,
    tableName: 'users',
})


