import express from 'express'
import BooksController from '../controllers/booksController'
import { isLogged } from '../middlewares/authenticate'

const route = express.Router()

route.post('/', isLogged, BooksController.createBook);
route.post('/edit', isLogged, BooksController.editBook);
route.post('/delete', isLogged, BooksController.deleteBook);
route.get('/all', isLogged, BooksController.allBooks);
route.get('/byUser', isLogged, BooksController.byUser);
route.post('/status', isLogged, BooksController.statusBook);


module.exports = route
