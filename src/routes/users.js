import express from 'express'
import UserController from '../controllers/usersController'
import { isLogged } from '../middlewares/authenticate'

const route = express.Router()

route.post('/', isLogged, UserController.createUser);
route.post('/login', UserController.login);
route.post('/edit', isLogged,  UserController.editUser);
route.post('/delete', isLogged, UserController.deleteUser);
route.get('/all', isLogged, UserController.allUsers)


module.exports = route
