import jwt from 'jsonwebtoken'


exports.authenticationToken = (usuario) => {
    let  tokenData = {
        usuario: usuario.correo,
        
    }


    const token = jwt.sign(tokenData, 'library2020', {
        expiresIn: 60 * 60 * 12 //One Hour
    })
    
    return 'library2020 ' + token;
}

exports.isLogged = async (req, res, next) => {

    try {
        const token = (req.headers['authorization'].split(' ')[1] || '')
        if (token) {
            // Token validation with the own key library2020
            jwt.verify(token, 'library2020', (err, decoded) => {
                if(err){

                    res.status(400).json({
                        resp: 400,
                        description: 'Token inválido'
                    })
                    return
                }
                else {
                    next();
                    
                }
            })

        }else{
            res.status(400).json({
                resp: 400,
                description: 'Token requerido'
            })
            return;
        }
        

    } catch (error) {
        console.error('Authorization error: ', error)

        res.status(400).json({
            resp: 400,
            description: 'Token inválido o inexistente'
        })
        
        return next(new Error('Unauthorized: ', error))
    }
   
}

