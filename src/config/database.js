import { Sequelize } from 'sequelize'
import { sqlite } from '../config/environment'


const sequelize = new Sequelize(sqlite.database, sqlite.username, sqlite.password, {
    host: sqlite.host,
    dialect: 'sqlite',
    define: {
        timestamps: false
    },
    dialectOptions: {
        supportBigNumbers: true,

    },
    storage:'library.db',
    logging: false,
});

const operator = Sequelize.Op;
const model = Sequelize.Model;
const sq = Sequelize;

module.exports = {
  sequelize,
  operator,
  model,
  sq
}
// exports.sequelize =


